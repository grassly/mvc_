﻿using System;
using deVoid.Utils;
using UnityEngine;

public class AddButtonSignal : ASignal<ResourceType, int> { }
public class ScreenSwitchSignal : ASignal<IUIController> { }