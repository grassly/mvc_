using System.Collections.Generic;
using UnityEngine;


    [CreateAssetMenu]
    public class OnButtonPressedSO : ScriptableObject
    {
        private List<IGameEventListener> _listeners = new List<IGameEventListener>();

        public void InvokeListeners()
        {
            foreach (var listener in _listeners)
            {
                listener.InvokeEvent();
            }
        }

        public void RegisterListener(IGameEventListener listener) => 
            _listeners.Add(listener);

        public void UnregisterListener(IGameEventListener listener) =>
            _listeners.Remove(listener);
    }