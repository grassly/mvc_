﻿
  public interface IGameEventListener<T>
  {
    void InvokeEvent(T type);
  }

  public interface IGameEventListener<T, U>
  {
    void InvokeEvent(T t, U u);
  }
  
  public interface IGameEventListener
  {
    void InvokeEvent();
  }