using System;
using System.Runtime.Serialization;
using deVoid.Utils;
using UnityEngine;

public class AddMenuController : IUIController
{
    public UISwitcher _switcher { get; set; }
    private ResourcePool _pool;

    public AddMenuController(UISwitcher switcher, ResourcePool pool)
    {
        _switcher = switcher;
        _pool = pool;
    }
    public void Enter()
    {
        Signals.Get<AddButtonSignal>().AddListener(OnAddButtonPressed);
    }
    
    public void Exit()
    {
        Signals.Get<AddButtonSignal>().RemoveListener(OnAddButtonPressed);
    }
    
    private void OnAddButtonPressed(ResourceType type, int amount) => 
        _pool.Pool[type] += amount;
}