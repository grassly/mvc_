using System;
using System.Collections;
using System.Collections.Generic;
using deVoid.Utils;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : IUIController, IGameEventListener
{
    private MainMenuView _mainMenuView;
    public UISwitcher _switcher { get; set; }
    private ResourcePool _pool;

    private OnButtonPressedSO _resetButtonListenerHub;
    public MainMenuController(UISwitcher switcher, MainMenuView mainMenuView, ResourcePool resourcePool)
    {
        _switcher = switcher;
        _mainMenuView = mainMenuView;
        _pool = resourcePool;
        _resetButtonListenerHub = Resources.Load("Signals/ResetSignal") as OnButtonPressedSO;
    }

    public void Enter()
    {
        _resetButtonListenerHub.RegisterListener(this);
        PerformResourcesPanel();
    }

    private void PerformResourcesPanel()
    {
        //при н.у. здесь должны быть не дестрой и инстанциироваание,
        //а пул уже созданных яйчеек, которые просто обновляются в этом методе
        
        for (int i = 0; i < _mainMenuView.CellRoot.childCount; i++)
        {
           GameObject.Destroy(_mainMenuView.CellRoot.GetChild(i).gameObject);
        }
        
        foreach (var item in _pool.Pool)
        {
            GameObject prefab = Resources.Load("Cell", typeof(GameObject)) as GameObject;
            GameObject cell = MonoBehaviour.Instantiate(prefab, _mainMenuView.CellRoot);
            
            CellView cellView = cell.GetComponent<CellView>();
            
            cellView.AmountText.text = item.Value.ToString();
            cellView.NameText.text = Enum.GetName(typeof(ResourceType), item.Key);
        }
    }
    
    public void Exit()
    {
        _resetButtonListenerHub.UnregisterListener(this);
    }

    private void OnResetButtonPressed()
    {
        /*foreach (var item in _pool.Pool)
        {
            _pool.Pool[item.Key] = 0;
        }*/
        _pool.ResetPool();
        PerformResourcesPanel();
        Debug.Log("Perform reset");
    }

    public void InvokeEvent() => 
        OnResetButtonPressed();
}