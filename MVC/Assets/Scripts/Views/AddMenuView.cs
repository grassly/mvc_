using System;
using deVoid.Utils;
using UnityEngine;
using UnityEngine.UI;

public class AddMenuView : MonoBehaviour
{
    [SerializeField] private Button _addButton;
    [SerializeField] private InputField _addAmountInputField;
    [SerializeField] private Dropdown _resourceToAdd;
    
    private void Awake()
    {
      _addButton.onClick.AddListener(InvokeAddButtonPressListeners);
      DropdownPerformer.PerformDropdownOptions(_resourceToAdd);
    }

    private void InvokeAddButtonPressListeners()
    {
      Signals.Get<AddButtonSignal>().Dispatch((ResourceType)_resourceToAdd.value, int.Parse(_addAmountInputField.text));
      _addAmountInputField.text = "";
    }
}