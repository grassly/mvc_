﻿using System;
using deVoid.Utils;
using UnityEngine;

public class ScreenView : MonoBehaviour
{
  [SerializeField] private GameObject _menuScreen;
  [SerializeField] private GameObject _addScreen;
  [SerializeField] private GameObject _removeScreen;

  private void Awake() => 
    Signals.Get<ScreenSwitchSignal>().AddListener(SwitchPanelToAdd);

  private void SwitchPanelToAdd(IUIController type)
  {
    if (type is AddMenuController)
    {
      _addScreen.SetActive(true);
      _removeScreen.SetActive(false);
      _menuScreen.SetActive(false);
      Debug.Log("Add");
    }
    else if (type is RemoveMenuController)
    {
      _addScreen.SetActive(false);
      _removeScreen.SetActive(true);
      _menuScreen.SetActive(false);
    }
    else if (type is MainMenuController)
    {
      _addScreen.SetActive(false);
      _removeScreen.SetActive(false);
      _menuScreen.SetActive(true);
    }
  }

  private void OnDestroy() => 
    Signals.Get<ScreenSwitchSignal>().RemoveListener(SwitchPanelToAdd);
}