﻿using System;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    [SerializeField] private Button _resetButton;
    [SerializeField] private OnButtonPressedSO _resetButtonListenerHub;
    [SerializeField] private Transform _cellRoot;
    public Transform CellRoot => _cellRoot;
    private void Awake() => 
        _resetButton.onClick.AddListener(() => {        
            _resetButtonListenerHub.InvokeListeners();
        });
}