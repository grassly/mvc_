using System;
using UnityEngine;
using UnityEngine.UI;

public class RemoveMenuView : MonoBehaviour
{
    public Button removeButton;
    public InputField RemoveAmountInputField;
    public Dropdown ResourceToRemove;
    
    public Action OnRemoveButtonPressed;
    public void InvokeContextAction() => OnRemoveButtonPressed?.Invoke();
}